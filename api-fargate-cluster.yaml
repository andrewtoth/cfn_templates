#====================================================================================================
#
# Name        : <project>-<env>-<workload>
# Description : ECS cluster running workload on fargate + ALB autoscale 
# Author      : Andrew Toth
# Date        : 26-09-2019
# Version     : 1.0
# Deploy      : aws cloudformation create-stack --stack-name ecs-fargate-cluster --template-body file://ecs-fargate-cluster.yml --region ap-southeast-2 --capabilities CAPABILITY_NAMED_IAM
#====================================================================================================
AWSTemplateFormatVersion: '2010-09-09'
Description: ECS cluster running workload on fargate + ALB autoscale
#====================================================================================================
#                                              Metadata
#====================================================================================================
#Metadata:
#====================================================================================================
#                                             Parameters
#====================================================================================================
Parameters:
  # global paramaters, same in all accounts
  ServiceName:
    Type: String
    Description: The name of the ECS cluster to create
    Default: api
  Image:
    Type: String
    # Update with the Docker image. "You can use images in the Docker Hub registry or specify other repositories (repository-url/image:tag)."
    Default: yeasy/simple-web
  ContainerPort:
    Type: Number
    Default: 80
  TaskCPU:
    Type: String
    Description: The number of cpu units the Amazon ECS container agent will reserve for the container. (1024 = 1 full CPU)
    Default: 256
  TaskMemory:
    Type: String 
    Description: The amount (in MiB) of memory to present to the container. If your container attempts to exceed the memory specified here, the container is killed
    Default: 0.5GB
  LoadBalancerPort:
    Type: Number
    Default: 443
  HealthCheckPath:
    Type: String
    Default: /
  # for autoscaling
  MinContainers:
    Type: Number
    Default: 1
  # for autoscaling
  MaxContainers:
    Type: Number
    Default: 10
  # target CPU utilization (%)
  AutoScalingTargetValue:
    Type: Number
    Default: 50

  PritunlSecurityGroup:
    Type: String 
    Description: The VPN Security group
    Default: 'sg-0e46eae0c0b9e2080'
#====================================================================================================
#                                              Mappings
#====================================================================================================
# environment specific items, cleaner than passing in a bunch of paramaters
Mappings:
  Accounts:
    "12345":
      Environment: dev      
      VPC: vpc-01633a3aff79afa28
      PublicSubnetA: subnet-0639a8d624b3189ad
      PublicSubnetB: subnet-041517e4a550bc63d
      PublicSubnetC: subnet-0cc84254d556389a5
      PrivateSubnetA: subnet-0e7bc437f4452b6d7
      PrivateSubnetB: subnet-0a313f56292b5d867
      PrivateSubnetC: subnet-04684663f4a93e395
      Certificate: arn:aws:acm:us-east-1:12345:certificate/71e56cfb-d667-4d9e-8097-236acd4a6f64
    "12345":
      Environment: staging      
      VPC: vpc-06dc778f9681de576
      PublicSubnetA: subnet-0a0b905d6f86b4619
      PublicSubnetB: subnet-044f15d0ef999e8fa
      PublicSubnetC: subnet-0e415cfdee74a6cda
      PrivateSubnetA: subnet-043dc5c7bd484f16e
      PrivateSubnetB: subnet-02a827b84888570e5
      PrivateSubnetC: subnet-0f58c13ef624a6eb9
      Certificate: arn:aws:acm:us-east-1:12345:certificate/559ac559-435f-4c29-8e8a-d941e1656218
    "12345":
      Environment: prod      
      VPC: vpc-004cffb271bec2ce9
      PublicSubnetA: subnet-085b394f26aa1bc17
      PublicSubnetB: subnet-03bc21be05c5c1935
      PublicSubnetC: subnet-088ba9820d7fc2c60
      PrivateSubnetA: subnet-06e4891807460f214
      PrivateSubnetB: subnet-018715402a3fde750
      PrivateSubnetC: subnet-0bba6ecd5f6244f3c
      Certificate: arn:aws:acm:us-east-1:12345:certificate/df55df34-4b3b-4186-b00d-05f6d496cc8d

#!FindInMap [Accounts, !Ref "AWS::AccountId", "VPC"]      
#====================================================================================================
#                                             Conditions
#====================================================================================================

#====================================================================================================
#                                              Resources
#====================================================================================================
Resources:
  Cluster:
    Type: AWS::ECS::Cluster
    Properties:
      ClusterName: !Ref ServiceName
  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    # Makes sure the log group is created before it is used.
    DependsOn: LogGroup
    Properties:
      # Name of the task definition. Subsequent versions of the task definition are grouped together under this name.
      Family: !Ref ServiceName
      # awsvpc is required for Fargate
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: !Ref TaskCPU
      Memory: !Ref TaskMemory
      ExecutionRoleArn: !Ref ExecutionRole
      # "The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that grants containers in the task permission to call AWS APIs on your behalf."
      TaskRoleArn: !Ref TaskRole
      
      ContainerDefinitions:
        - Name: !Ref ServiceName
          Image: !Ref Image
          PortMappings:
            - ContainerPort: !Ref ContainerPort
          Environment:
            - Name: 'environment'
              Value: !FindInMap [Accounts, !Ref "AWS::AccountId", "Environment"]
          # Send logs to CloudWatch Logs
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs

  # A role needed by ECS
  ExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', [!Ref ServiceName, ExecutionRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'
  # A role for the containers
  TaskRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', [!Ref ServiceName, TaskRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      # ManagedPolicyArns:
      #   -
      # Policies:
      #   -
  # A role needed for auto scaling
  AutoScalingRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', [!Ref ServiceName, AutoScalingRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole'
  ContainerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: !Join ['', [!Ref ServiceName, ContainerSecurityGroup]]
      VpcId: !FindInMap [Accounts, !Ref "AWS::AccountId", "VPC"]     
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: !Ref ContainerPort
          ToPort: !Ref ContainerPort
          SourceSecurityGroupId: !Ref LoadBalancerSecurityGroup
        - IpProtocol: tcp
          FromPort: !Ref ContainerPort
          ToPort: !Ref ContainerPort
          SourceSecurityGroupId: !Ref PritunlSecurityGroup
  LoadBalancerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: !Join ['', [!Ref ServiceName, LoadBalancerSecurityGroup]]
      VpcId: !FindInMap [Accounts, !Ref "AWS::AccountId", "VPC"]      
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: !Ref LoadBalancerPort
          ToPort: !Ref LoadBalancerPort
          CidrIp: 0.0.0.0/0
  Service:
    Type: AWS::ECS::Service
    # This dependency is needed so that the load balancer is setup correctly in time
    DependsOn:
      - ListenerHTTPS
    Properties: 
      ServiceName: !Ref ServiceName
      Cluster: !Ref Cluster
      TaskDefinition: !Ref TaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 1
      # This may need to be adjusted if the container takes a while to start up
      HealthCheckGracePeriodSeconds: 30
      LaunchType: FARGATE
      NetworkConfiguration: 
        AwsvpcConfiguration:
          # change to DISABLED if you're using private subnets that have access to a NAT gateway
          AssignPublicIp: DISABLED
          Subnets:
            - !FindInMap [Accounts, !Ref "AWS::AccountId", "PrivateSubnetA"]
            - !FindInMap [Accounts, !Ref "AWS::AccountId", "PrivateSubnetB"]
            - !FindInMap [Accounts, !Ref "AWS::AccountId", "PrivateSubnetC"]
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      LoadBalancers:
        - ContainerName: !Ref ServiceName
          ContainerPort: !Ref ContainerPort
          TargetGroupArn: !Ref TargetGroup
  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 10
      # will look for a 200 status code by default unless specified otherwise
      HealthCheckPath: !Ref HealthCheckPath
      HealthCheckTimeoutSeconds: 5
      UnhealthyThresholdCount: 2
      HealthyThresholdCount: 2
      Name: !Join ['', [!Ref ServiceName, TargetGroup]]
      Port: !Ref ContainerPort
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: '60' # default is 300
      TargetType: ip
      VpcId: !FindInMap [Accounts, !Ref "AWS::AccountId", "VPC"]
  ListenerHTTPS:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - TargetGroupArn: !Ref TargetGroup
          Type: forward
      LoadBalancerArn: !Ref LoadBalancer
      Port: !Ref LoadBalancerPort
      Protocol: HTTPS #or HTTPS
      Certificates:
        - CertificateArn: !FindInMap [Accounts, !Ref "AWS::AccountId", "Certificate"]
  LoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        # this is the default, but is specified here in case it needs to be changed
        - Key: idle_timeout.timeout_seconds
          Value: '60'
      Name: !Join ['', [!Ref ServiceName, LoadBalancer]]
      # "internal" is also an option
      Scheme: internet-facing
      SecurityGroups:
        - !Ref LoadBalancerSecurityGroup
      Subnets:
        - !FindInMap [Accounts, !Ref "AWS::AccountId", "PublicSubnetA"]
        - !FindInMap [Accounts, !Ref "AWS::AccountId", "PublicSubnetB"]
        - !FindInMap [Accounts, !Ref "AWS::AccountId", "PublicSubnetC"]

  #DNSRecord:
  #  Type: AWS::Route53::RecordSet
  #  Properties:
  #    HostedZoneName: !Join ['', [!Ref HostedZoneName, .]]
  #    Name: !Join ['', [!Ref Subdomain, ., !Ref HostedZoneName, .]]
  #    Type: A
  #    AliasTarget:
  #      DNSName: !GetAtt LoadBalancer.DNSName
  #      HostedZoneId: !GetAtt LoadBalancer.CanonicalHostedZoneID

  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ['', [/ecs/, !Ref ServiceName, TaskDefinition]]

  AutoScalingTarget:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    Properties:
      MinCapacity: !Ref MinContainers
      MaxCapacity: !Ref MaxContainers
      ResourceId: !Join ['/', [service, !Ref Cluster, !GetAtt Service.Name]]
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs
      # "The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that allows Application Auto Scaling to modify your scalable target."
      RoleARN: !GetAtt AutoScalingRole.Arn

  AutoScalingPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: !Join ['', [!Ref ServiceName, AutoScalingPolicy]]
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref AutoScalingTarget
      TargetTrackingScalingPolicyConfiguration:
        PredefinedMetricSpecification:
          PredefinedMetricType: ECSServiceAverageCPUUtilization
        ScaleInCooldown: 10
        ScaleOutCooldown: 10
        # Keep things at or lower than 50% CPU utilization, for example
        TargetValue: !Ref AutoScalingTargetValue
  
  PrivateNameSpace:
    Type: AWS::ServiceDiscovery::PrivateDnsNamespace
    Properties: 
      Description: !Sub "${ServiceName}-NameSpace"
      Name: !Ref ServiceName
      Vpc: !FindInMap [Accounts, !Ref "AWS::AccountId", "VPC"]
#====================================================================================================
#                                               Outputs
#====================================================================================================
Outputs:
  Endpoint:
    Description: ApiEndpoint
    #Value: !Join ['', ['https://', !Ref DNSRecord]]
    Value: !GetAtt LoadBalancer.DNSName
    Export:
      Name: !Sub "${ServiceName}-ApiEndpoint"
  ApicontainerSG:
    Description: ContainerSG
    Value: !Ref ContainerSecurityGroup
    Export:
      Name: !Sub "${ServiceName}-ContainerSG"
  ApiClusterName:
    Description: ApiClusterName
    Value: !Ref ServiceName
    Export: 
      Name: !Sub "${ServiceName}-ClusterName"
  PrivateNameSpace:
    Description: PrivateNameSpace
    Value: !GetAtt PrivateNameSpace.Id
    Export:
      Name: !Sub "${ServiceName}-NameSpace"

